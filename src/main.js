import Router from './Router.js';
import data from './data.js';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js'
import PizzaForm from './pages/PizzaForm.js'

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
    
Router.menuElement = document.querySelector('.mainMenu');


//Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate(window.location.pathname); // affiche la liste des pizzas
document.querySelector('.logo').innerHTML += "<small>les pizzas c'est la vie</small>"; 

//document.querySelector('.pizzaListLink').setAttribute("class", "pizzaListLink active"); 
document.querySelector('.newsContainer').setAttribute("style", "display:''");

function handleClick( event ) {
	event.preventDefault(); // empêche le rechargement de la page
    document.querySelector('.newsContainer').setAttribute("style", "display:none");
}

document.querySelector('.closeButton').addEventListener('click', handleClick);

window.onpopstate = () => console.log(document.location);